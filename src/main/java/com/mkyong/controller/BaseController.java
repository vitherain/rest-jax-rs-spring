package com.mkyong.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by V�t on 20. 1. 2016.
 */
@Controller
public class BaseController {

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String getSamplePage() {
        return "home";
    }
}
